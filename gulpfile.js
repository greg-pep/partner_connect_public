const gulp = require('gulp');
const stylus = require('gulp-stylus');
const browserSync = require('browser-sync').create();

// Compile Stylus
gulp.task('compileStylus', function() {
    return gulp.src('app/styl/styles.styl')
    .pipe(stylus({
        // compress: true
    }))
    .pipe(gulp.dest('app/css'))
    .pipe(browserSync.stream());
});

// Watch
gulp.task('watch', function() {
    browserSync.init({
        server: {
           baseDir: 'app'
        }
    });
    gulp.watch('app/styl/**/*.styl', gulp.series(['compileStylus']))
    gulp.watch('app/*.html').on('change',browserSync.reload);
    gulp.watch('app/js/**/*.js').on('change', browserSync.reload);
});
