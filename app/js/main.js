const body = document.body;
const accodionListsArray = body.querySelectorAll('.accordion');

// DROPDOWN EVENTS
const dropdownCtnrArray = body.querySelectorAll('.dropdown-ctnr');
dropdownCtnrArray.forEach(function(dropdownCtnr) {
    dropdownBtn = dropdownCtnr.querySelector('.dropdown-btn');
    dropdownBtn.addEventListener("click", (e) => {
        if (dropdownCtnr.classList.contains('expand')) {
            dropdownCtnr.classList.remove('expand'); 
        }
        else {
            dropdownCtnrArray.forEach(function(dropdownCtnr) {
                dropdownCtnr.classList.remove('expand');
            })
            dropdownCtnr.classList.add('expand');
        }
       
        body.addEventListener("click", (f) => {
            if (!f.target.closest('.dropdown-ctnr')) {
                dropdownCtnr.classList.remove('expand');
            }
        })
        event.preventDefault();
    })
})

// EXPANDABLE CTNR
const maxNbItem = 3;
const expandableCtnrArray = body.querySelectorAll('.expandable-ctnr');
expandableCtnrArray.forEach(function(expandableCtnr) {
    const expandableItemArray = expandableCtnr.querySelectorAll('.expandable-item');
    const expandableItemNb = expandableItemArray.length;

    expandableItemArray.forEach(function(expandableItem, index) {
        if (expandableItemNb > maxNbItem) {
            if (index >= maxNbItem) {
                expandableItem.classList.add("hidden");
            }
        }
    })

    if (expandableItemNb > maxNbItem) {
        const expandBtnCtnr = document.createElement('div');
        expandBtnCtnr.className = "mt2_";
        const expandBtn = document.createElement('button');
        expandBtn.className = "btn expandable-btn btn-secondary thirdColor maauto";
        expandBtn.innerHTML = 'Display more <i class="fas fa-chevron-down ml0_" aria-hidden="true"></i>';
        expandBtnCtnr.appendChild(expandBtn);
        expandableCtnr.appendChild(expandBtnCtnr);

        expandBtn.addEventListener("click", (e) => {
            expandableItemArray.forEach(function(expandableItem) {
                if (expandableItem.classList.contains('hidden')) {
                    expandableItem.classList.remove('hidden');
                }
            })
            expandBtnCtnr.remove();
        })
    }
})


// POPIN EVENTS
const popinBtnArray = body.querySelectorAll('.popin-btn')
const overflow = body.querySelector('.overflow');
// Click on button which is linked to popin
popinBtnArray.forEach(function(popinBtn) {
    popinBtn.addEventListener("click", (e) => {
        if (e.target.classList.contains('contact-btn')) {
            e.target.classList.add('activBtn');
        }
        overflow.classList.add('visible');
        body.classList.add('noScroll');

        // TEMP : Inject popin content
        popinTarget = e.target.closest('.popin-btn').getAttribute('data-popin-target');
        popinTargetItem = document.getElementById(popinTarget);
        popinsArray = overflow.querySelectorAll('.popin');
        if (popinTargetItem != null) {
            popinsArray.forEach(function(popin) {
                popin.classList.remove('visible');
            })
        }     
        popinTargetItem.classList.add('visible');

        // Closing function
        function closePopin() {
            popinTargetItem.classList.remove('visible');
            overflow.classList.remove('visible');
            body.classList.remove('noScroll');

            if(popinBtn.classList.contains('activBtn')) {
                popinBtn.classList.remove('activBtn');
            }
        }

        // Close with button
        popinCloseBtnsArray = popinTargetItem.querySelectorAll('.popin-close-btn');
        popinCloseBtnsArray.forEach(function(popinCloseBtn) {
            if (popinCloseBtn != null) {
                popinCloseBtn.addEventListener("click", (e) => {
                    closePopin();
                })
            }
        })

        // Close with overflow
        overflow.addEventListener("click", (e) => {
            if (!e.target.closest('.popin')) {
                closePopin();
            }
        })

        event.preventDefault();
    })
})

// ACCORDION EVENTS
// Click on button for fold/unfold content
accodionListsArray.forEach(function(accordion) {
    accordionLink = accordion.querySelector('.accordion-btn');
    accordionContent = accordion.querySelector('.accordion-content');
    
    accordionLink.addEventListener("click", (e) => {
        if (accordion.classList.contains('open')) {
            accordion.classList.remove('open');
        }
        else {
            accordion.classList.add('open');
        }
    })
})

// WINDOWS EVENTS
// Delete classes on open menu when resizing window
window.addEventListener("resize", (e) => {
    windowWidth = window.innerWidth;
    if(windowWidth >= 768 && !(overflow.classList.contains('visible'))) {
        navMenu.classList.remove('openNavMenu');
        navMenu.classList.remove('scrollDown');
        body.classList.remove('noScroll');
    }
})
