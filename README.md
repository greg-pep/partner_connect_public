# Présentation
Ce projet est la partie front du portail Partner-connect.

---
## Architecture
Ce projet est conçu sur le modèle de l'architecture IT-CSS.
La méthodologie de nomenclature utilisée se base sur OOCSS.

---
## Design

Les wireframes Balsamiq, définissant la structure UX des intégrations, sont visibles à l'url :
https://balsamiq.cloud/sqq183/ppupnbc

Les maquettes Figma à partir desquelles les intégrations ont été réalisées sont visibles à l'url :
https://www.figma.com/file/9AtVhI2BSC6qT2f1gQ4iGi/05---Partner-connect-(with-DS)?node-id=1%3A3

---
## Styles
Les styles CSS sont compilés avec Stylus. Les fichiers stylus non minifiés sont dans le dossier "styl".

Pour gérer les styles de base des éléments les fichiers Stylus suivants ont été utilisés :
- [reset.styl] est trouvable à cette url : https://gist.github.com/samueleaton/41f76edf1ee49f2cbf5d
- [normalize.styl] est trouvable à cette url : https://github.com/bymathias/normalize.styl

Les fichiers .styl préfixés par '_' sont ajouté automatiquement à la compilation.
En revanche, les fichiers sans ce préfixe doivent être ajoutés manuellement.

---
## Javascripts
Les scripts JS se trouvent dans le dossier JS et sont appelés à la fin des fichiers html
Ils ne sont ni concaténés, ni minifiés, et sont tous dans le fichier [main.js]

---
## Images
Les images du site sont optimisées pour améliorer les performances.
Quand c'était possible, les images dont la résolution ne doit pas être limitée (les logos par exemple) ont été ajoutés au format SVG

---
## Accessibilité
Afin d'optimiser l'accessibilité, j'ai mis en place les pratiques suivantes :
- Les balises HTML et HTML5 adéquates avec la nature du contenu.
- L'attribut "role" lorsqu'il est pertinent sur une balise.
- L'attribut "aria-label" ainsi que l'attribut "title" pour décrire et rendre visible l'action des boutons et des liens, quand ils ne contiennent pas un texte explicite définissant cette action.
- Les textes alternatifs sur toutes les images dans le HTML.

---
## Environnement
Afin de gérer plus simplement les tâches du projet (comme la compilation auto des fichiers .styl),
un environnement local de dev ont été crée avec Gulp (dont la config se trouve dans gulpfile.js).

Afin de lancer l’automatisation du projet, il est possible d’utiliser la tâche watch, avec la commande "gulp watch".
Cela ouvrira automatiquement le projet sur le port 3000, à l'aide de browser-sync.

Cependant, dans la mesure où aucune ressource ne nécessite de serveur,
il est possible de consulter le projet en "drag-&-dropant" le fichier index.html dans un navigateur.

---
## Dépendances NPM
Pour installer les dépendances, lancer 'npm install'.

Les dépendances utilisées :
- gulp 4.0.2          : https://www.npmjs.com/package/gulp
- gulp-stylus 2.7.0   : https://www.npmjs.com/package/gulp-stylus
- browser-sync 2.26.7 : https://www.npmjs.com/package/browser-sync

---
## Utilisation de gulp
- La tâche 'gulp compileStylus' lance la compilation de tous les fichiers.
- La tâche 'gulp watch' lance le serveur Browser-sync, la compilation Stylus et le suivi de modifications.
